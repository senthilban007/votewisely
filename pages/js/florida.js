/* Main Section for Issues in React */

// import React from 'react';

class FloridaSection extends React.PureComponent {

  render() {
    return (
        <div id="details">
          <p>{this.props.session}</p>
          <p>{this.props.legislatureName}</p>
          <a href={this.props.legislature}>Legislature</a>
          <p>{this.props.chambers}</p>
          <p>{this.props.seats}</p>
        </div>
    );
  }

}

class IssueList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="issues-list">

        <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <br /><br />
            <p>Population: 20.98 million</p>
            <p><a href="representatives.html">Representatives</a>:</p>
              <ul>
                <li>27 Representatives in total.</li>
                <li>Rick Scott</li>
                <li>Marco Rubio</li>
              </ul>
            <p>Recent Statements (Credits to ProPublica API and Congress.gov):</p>
              <ul>
                <li><a href="https://yoho.house.gov/media-center/in-the-news/marco-rubio-ted-yoho-champion-the-saudi-nuclear-nonproliferation-act">Marco Rubio, Ted Yoho Champion the Saudi Nuclear Nonproliferation Act</a></li>
                <li><a href="https://www.rubio.senate.gov//public/index.cfm/press-releases?ContentRecord_id=006E4968-4D7D-4251-867F-EEF20A69D4F7">Senators Rubio, Gillibrand and Representatives Cohen, Desaulnier Reintroduce Bipartisan Bill to Protect Drivers from Fatal Tractor Trailer Truck Accidents</a></li>
                <li><a href="https://yoho.house.gov/media-center/in-the-news/juan-guaido-defiantly-returns-to-caracas">Juan Guaido defiantly returns to Caracas</a></li>
                <li>Find more at <a href="issues.html">Issues</a></li>
              </ul>
            <p>Part of United States since: 1845</p>
            <p>Governor: Ron DeSantis</p>
          </div>
        </div>
        <br></br>
      </div>

    );
  }

}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Florida</h2>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/florida2.jpg" id="banner" />
        <Purpose />
        <hr />
        <IssueList />
      </div>
    );
  }
}


ReactDOM.render(
  <Main />,
  document.getElementById('react-florida')
);
