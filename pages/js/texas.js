/* Main Section for Issues in React */

// import React from 'react';

class IssueList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="issues-list">

        <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <br /><br />
            <p>Population: 28.3 million</p>
            <p><a href="representatives.html">Representatives</a>:</p>
              <ul>
                <li>36 Representatives in total.</li>
                <li>Ted Cruz</li>
                <li>John Cornyn</li>
              </ul>
            <p>Recent Statements (Credits to ProPublica API and Congress.gov):</p>
              <ul>
                <li><a href="https://gonzalez.house.gov/media/press-releases/congressman-gonzalez-promotes-legacy-former-congressman-e-kika-de-la-garza">Congressman Gonzalez Promotes the Legacy of Former Congressman E. Kika De La Garza</a></li>
                <li><a href="https://castro.house.gov/media-center/press-releases/castro-welcomes-500000-utsa-research-water-and-electricity-distribution">Castro Welcomes $500,000 to UTSA for Research on Water and Electricity Distribution Systems</a></li>
                <li><a href="https://www.cruz.senate.gov?p=press_release&id=4351">Sens. Cruz, Cortez Masto Announce Intention to Reintroduce Bipartisan E-FRONTIER Act</a></li>
                <li>Find more at <a href="issues.html">Issues</a></li>
              </ul>
            <p>Part of United States since: 1845</p>
            <p>Governor: Greg Abbott</p>
          </div>
        </div>
        <br></br>
      </div>

    );
  }

}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Texas</h2>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/texas2.png" id="banner" />
        <Purpose />
        <hr />
        <IssueList />
      </div>
    );
  }
}


ReactDOM.render(
  <Main />,
  document.getElementById('react-texas')
);
