$("path, circle, .red-state").hover(function(e) {
  $('#info-box').css('display','block');
  $('#info-box').html($(this).data('info'));
});

$("path, circle").mouseleave(function(e) {
  $('#info-box').css('display','none');
});

$("path, circle").click(function(e){
  switch($(this).data('state')) {
    case 'TX' :
      window.open('texas.html');
      break;
    case 'FL' :
      window.open('florida.html');
      break;
    case 'CA' :
      window.open('california.html');
      break;
    default :
      alert("You clicked on " + $(this).data('state') + "\nPage coming soon!");
  }
});

$(document).mousemove(function(e) {
  $('#info-box').css('top',e.pageY-$('#info-box').height()-30);
  $('#info-box').css('left',e.pageX-($('#info-box').width())/2);
}).mouseover();

var ios = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
if(ios) {
  $('a').on('click touchend', function() { 
    var link = $(this).attr('href');   
    window.open(link,'_blank');
    return false;
  });
}