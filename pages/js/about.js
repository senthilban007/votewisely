/* Main Section for About in React
 * Imports Member Cards: member-card.js
 */

// import React from 'react';

var groupMembers = {"Jesse Martinez" : 0, "JesusVasq":0, "AmitiB":0,"Jie Hao Liao":0,"Lisa Barson":0,"Fan Yang":0}
var groupMembers_issues = {"jezze4" : 0, "JesusVasq":0, "AmitiB":0,"liaojh1998":0,"lisab":0,"BertrandsParadogs":0}

function myFunction(data) {
  var i;
  for (i = 0; i < data.length; i++){
    if(data[i].author_name.toString() in groupMembers){
      groupMembers[data[i].author_name.toString()] += 1;
      }
    } 
    return groupMembers;
   // The function returns the product of p1 and p2
}

function myFunction2(data) {
  var i;
  for (i = 0; i < data.length; i++){
    if(data[i].author.username.toString() in groupMembers_issues){
      groupMembers_issues[data[i].author.username.toString()] += 1;
      }
    } 
    return groupMembers_issues;
   // The function returns the product of p1 and p2
}

class Member extends React.PureComponent {

  render() {
    return (
      <div id="card" class="container">
        <div class="image-cropper">
          <img src={this.props.image} id="selfie"/>
        </div>
        <div id="details">
          <h2>{this.props.name}</h2>
          <p>{this.props.role}</p>
          <p>{this.props.bio}</p>
        </div>
      </div>
    );
  }

}

class MemberList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="members-list">
        <h2 class="title-text">Meet the Engineers</h2>
        <div class="row">
          <div class="col-xs-6 col-md-4 no-padding">
            <Member 
              name="Jesse Martinez"
              role="Front-end Engineer"
              bio="5th-Year CS Student. Enjoys gaming and has a thing against Apple."
              image="../imgs/members/jezze.jpg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member 
              name="Lisa Barson"
              role="Front-end Engineer"
              bio="Junior CS Student. Enjoys to play volleyball and go hiking!"
              image="../imgs/members/lisa.jpeg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Amiti Busgeeth"
              role="Front-end Engineer"
              bio="Junior CS Student. Cats and kittens are adorable!"
              image="../imgs/members/amiti.jpg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Jie Hao Liao"
              role="Full-Stack Engineer"
              bio="Junior CS Student. Enjoys K-POP!! (Dancing and singing!)"
              image="../imgs/members/jay.jpeg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Fan Yang"
              role="Back-end Engineer"
              bio="Junior CS Student. Can be seen around campus on his 'Hoverboard'"
              image="../imgs/members/fan.png"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Jesus Vasquez"
              role="Back-end Engineer"
              bio="Senior CS Student. Cannot wait for Games of Thrones Season Finale"
              image="../imgs/members/jesus.jpg"
            />
          </div>
        </div>

      </div>
    );
  }

}

class Stats extends React.PureComponent {

   constructor(props) {
    super(props);
    this.state = {
      len: 0,
      len2: 0
    };
  }

  componentDidMount() {

    fetch("https://gitlab.com/api/v4/projects/11046118/repository/commits?per_page=999999")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            len: result.length
          });
          
          console.log(myFunction(result));
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            len: 0
          });
        },

      );


    fetch("https://gitlab.com/api/v4/projects/11046118/issues?per_page=999999")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            len2: result.length
          });
          console.log(myFunction2(result));
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            len2: 0
          });
        }
      );

  }


  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text">The Statistics</h2>
        <div class="container">
          <table class="table table-cover">
            <thead>
              <tr>
               
                <th scope="col">Names</th>
                <th scope="col">Commits</th>
                <th scope="col">Issues</th>


              </tr>
            </thead>
            <tbody>
              <tr>
             
                <td>Jezze Martinez</td>
                <td>{groupMembers["Jesse Martinez"]}</td>
                <td>2</td>
              </tr>
              <tr>
              
                <td>Lisa Barson</td>
                <td>{groupMembers["Lisa Barson"]}</td>
                <td>2</td>
              </tr>
              <tr>
              
                <td>Amiti Busgeeth</td>
                <td>{groupMembers["AmitiB"]}</td>
                <td>4</td>
              </tr>
              <tr>
              
                <td>Jie Hao Liao</td>
                <td>{groupMembers["Jie Hao Liao"]}</td>
                <td>13</td>
               </tr>
              <tr>
             
                <td>Fan Yang</td>
                <td>{groupMembers["Fan Yang"]}</td>
                <td>1</td>
              </tr>
              <tr>
         
                <td>Jesus Vasquez</td>
                <td>{groupMembers["JesusVasq"]}</td>
                <td>2</td>
              </tr>
              <tr>
                <td><strong>Total</strong></td>
                <td>{this.state.len}</td>
                <td>{this.state.len2}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

class Purpose extends React.PureComponent {

 
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Our Mission</h2>
        <p id="mission-statement"> 
          Our Mission is to help citizens be politically informed 
          and get excited to vote by providing them with unbiased 
          information on politicians and issues based on their location
        </p>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/about_banner.jpg" id="banner" />
        <hr />
        <Purpose />
        <hr />
        <MemberList />
        <hr />
        <Stats />
        <div id="links">  
        <p>
          <a href="https://gitlab.com/votewiselyengineers/votewisely">GitLab Link</a>
        </p>
        <p>
          <a href="https://documenter.getpostman.com/view/6804368/S11NMwfE">Postman</a>
        </p>
        <p>
          Tools used: GitLab, Slack, React, AWS, Photoshop, Python, Bootstrap, Postman, Flask
        </p>
        </div>
      </div>
    );
  }
}


ReactDOM.render(
  <Main />, 
  document.getElementById('react-about')
);
