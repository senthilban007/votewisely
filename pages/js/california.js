/* Main Section for Issues in React */

// import React from 'react';

class CaliforniaSection extends React.PureComponent {

  render() {
    return (
        <div id="details">
          <p>{this.props.session}</p>
          <p>{this.props.legislatureName}</p>
          <a href={this.props.legislature}>Legislature</a>
          <p>{this.props.chambers}</p>
          <p>{this.props.seats}</p>
        </div>
    );
  }

}

class IssueList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="issues-list">

        <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <br /><br />
            <p>Population: 39.54 million</p>
            <p><a href="representatives.html">Representatives</a>:</p>
              <ul>
                <li>53 Representatives in total.</li>
                <li>Kamala Harris</li>
                <li>Dianne Feinstein</li>
              </ul>
            <p>Recent Statements (Credits to ProPublica API and Congress.gov):</p>
              <ul>
                <li><a href="https://eshoo.house.gov/news-stories/press-releases/over-125-local-governments-and-130-public-utilities-in-47-states-voice-support-for-eshoo-bill-to-empower-local-5g-deployment/">OVER 125 LOCAL GOVERNMENTS AND 130 PUBLIC UTILITIES IN 47 STATES VOICE SUPPORT FOR ESHOO BILL TO EMPOWER LOCAL 5G DEPLOYMENT</a></li>
                <li><a href="https://cisneros.house.gov/media/press-releases/rep-cisneros-submits-bipartisan-letter-support-us-dot-seeking-funds-5760">Rep. Cisneros Submits Bipartisan Letter of Support to the U.S. DOT Seeking Funds for the 57/60 Project by LA Metro</a></li>
                <li><a href="https://garamendi.house.gov/media/in-the-news/area-congressmen-join-together-provide-federal-support-proposed-sites-reservoir">Area congressmen join together to provide federal support for proposed Sites Reservoir</a></li>
                <li>Find more at <a href="issues.html">Issues</a></li>
              </ul>
            <p>Part of United States since: 1850</p>
            <p>Governor: Gavin Newsom</p>
          </div>
        </div>
        <br></br>
      </div>

    );
  }

}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">California</h2>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/california2.png" id="banner" />
        <Purpose />
        <hr />
        <IssueList />
      </div>
    );
  }
}


ReactDOM.render(
  <Main />,
  document.getElementById('react-california')
);
