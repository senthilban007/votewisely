#!/usr/bin/env python
import requests
import json
import traceback
import sys
from os.path import isfile
from googleapi import key


GOOGLE_API_URL = 'https://www.googleapis.com/civicinfo/v2/representatives'
GOOGLE_API_KEY = key
REPRESENTATIVES_METADATA_FILENAME = 'representatives_metadata.json'

ADDRESS = "Austin Texas"


def get_officials_metadata_from_url():
	global GOOGLE_API_URL
	#query_parameters = {'ADDRESS':'Austin, Texas','includeOffices':'true','levels':'country','roles':'headOfGovernments','key':GOOGLE_API}
	civic_request = requests.get(GOOGLE_API_URL+'?key='+GOOGLE_API_KEY+'&address='+ADDRESS);
	if civic_request.status_code != 200:
		return {'success':False,'content':None, 'error':'Bad status code ' + str(civic_request.status_code)}
	else:
            return {'success': True,
            'content': civic_request.json(),
            'error': None}

def get_officials_metadata(force_update=False):
    global REPRESENTATIVES_METADATA_FILENAME
    stored = isfile(REPRESENTATIVES_METADATA_FILENAME)
    if force_update or not stored:
        r = get_officials_metadata_from_url()
        if not r['success']:
            sys.stderr.write('%s\n' % r['error'])
            return None
        with open(REPRESENTATIVES_METADATA_FILENAME, 'w') as local_file:
            json.dump(r['content'], local_file)
        return r['content']
    try:
        with open(REPRESENTATIVES_METADATA_FILENAME, 'r') as local_file:
            return json.load(local_file)
    except:
        sys.stderr.write('---- Error while parsing JSON ----\n')
        traceback.print_exc()
        return None
