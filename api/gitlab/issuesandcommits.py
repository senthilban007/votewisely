#!/usr/bin/env python
import requests
import json
import traceback
import sys
from os.path import isfile


GITLAB_URL = 'https://gitlab.com/api/v4/projects/11046118/'
COMMIT_METADATA_FILENAME = 'commit_metadata.json'
ISSUES_METADATA_FILENAME = 'issues_metadata.json'

#curl --header "PRIVATE-TOKEN: contents of gitlab token" https://gitlab.com/api/v4/projects/11046118/repository/commits
def get_commits():
	global GITLAB_URL
	gl_commits = requests.get(GITLAB_URL+'repository/commits')
	if gl_commits.status_code != 200:
		return {'success':False,'content':None, 'error':'Bad status code ' + str(gl_commits.status_code)}
	else:
            return {'success': True,
            'content': len(gl_commits.json()),
            'error': None}


def get_commits_metadata(force_update=False):
    global COMMIT_METADATA_FILENAME
    stored = isfile(COMMIT_METADATA_FILENAME)
    if force_update or not stored:
        r = get_commits()
        if not r['success']:
            sys.stderr.write('%s\n' % r['error'])
            return None
        with open(COMMIT_METADATA_FILENAME, 'w') as local_file:
            json.dump(r['content'], local_file)
        return r['content']
    try:
        with open(COMMIT_METADATA_FILENAME, 'r') as local_file:
            return json.load(local_file)
    except:
        sys.stderr.write('---- Error while parsing JSON ----\n')
        traceback.print_exc()
        return None

#curl --header "PRIVATE-TOKEN: contents of gitlab token" https://gitlab.com/api/v4/projects/11046118/issues
def get_issues():
	global GITLAB_URL
	gl_issues = requests.get(GITLAB_URL+'issues')
	if gl_issues.status_code != 200:
		return {'success':False,'content':None, 'error':'Bad status code ' + str(gl_issues.status_code)}
	else:
            return {'success': True,
            'content': len(gl_issues.json()),
            'error': None}

def get_issues_metadata(force_update=False):
    global COMMIT_METADATA_FILENAME
    stored = isfile(COMMIT_METADATA_FILENAME)
    if force_update or not stored:
        r = get_issues()
        if not r['success']:
            sys.stderr.write('%s\n' % r['error'])
            return None
        with open(ISSUES_METADATA_FILENAME, 'w') as local_file:
            json.dump(r['content'], local_file)
        return r['content']
    try:
        with open(ISSUES_METADATA_FILENAME, 'r') as local_file:
            return json.load(local_file)
    except:
        sys.stderr.write('---- Error while parsing JSON ----\n')
        traceback.print_exc()
        return None