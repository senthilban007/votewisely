SHELL := /bin/bash

all: clean check deploy

pipeline-keys:
	@echo "Adding API keys for deployment"
	echo "key = '$$CIVICFEED_APIKEY'" > api/civicfeed/civicfeed_key.py

api-unittest:
	pytest --cov=api tests/api/

check:
	@echo "Checking if all files in FILES exist."
	@while read file; \
		do if [[ -n $$file ]] && [[ $${file::1} != "#" ]] \
				&& [[ ! -f $$file ]] && [[ ! -d $$file ]]; then \
			echo "$$file: No such file or directory"; \
			exit 2; \
		fi \
	done < .gitlab-ci/FILES
	@echo "Success, all files in FILES exist!"

deploy:
	@echo "Copying files to .gitlab-ci/files-to-deploy folder for deployment"
	@.gitlab-ci/deploy.sh .gitlab-ci/FILES
	@echo "Success!"

clean:
	rm -rf .gitlab-ci/files-to-deploy
